package main;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

public class TestRectangle {

    private Rectangle rectangle;

    @BeforeEach
    public void init() {
        int width = 3;
        int height = 4;
        rectangle = new Rectangle(width, height);
    }

    @Test
    public void notZeroValues() {
        assertTrue(rectangle.getWidth() > 0, "width > 0? It is true?");
        assertTrue(rectangle.getHeight() > 0, "height > 0? It is true?");
    }

    @Test
    public void notNegativeValues() {
        assertFalse(rectangle.getWidth() < 0, "width < 0? It is false?");
        assertFalse(rectangle.getHeight() < 0, "height < 0? It is false?");
    }

    @Test
    public void checkAreaRectangle() {
        int expectedArea = rectangle.getWidth() * rectangle.getHeight();
        String message = rectangle.getWidth() + " * " + rectangle.getHeight() + " = " + expectedArea;
        assertEquals(expectedArea, rectangle.getArea(), message);
    }

    @Test
    public void checkCircuitRectangle() {
        int expectedCircuit = (rectangle.getWidth() * 2) + (rectangle.getHeight() * 2);
        String message = String.format("(2 * %d) + (2 * %d) = %d?",
                rectangle.getHeight(), rectangle.getWidth(), expectedCircuit);
        assertEquals(expectedCircuit, rectangle.getCircuit(), message);
    }

    @Test
    public void checkParametersRectangle() {
        assertTrue(rectangle.getHeight() >= rectangle.getWidth() ||
                rectangle.getWidth() >= rectangle.getHeight(), "Check parameters..."
        );
    }
}
